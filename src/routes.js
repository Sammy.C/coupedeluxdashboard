/*!

=========================================================
* Argon Dashboard React - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import Index from "views/Index.js";
import Profile from "views/examples/Profile.js";
import Login from "views/examples/Login.js";
import Barber from "views/examples/Barber.js";
import Bookings from "views/examples/Tables.js"
import Services from "views/examples/Service"

var routes = [
  {
    path: "/dashboard",
    name: "Dashboard",
    icon: "ni ni-chart-bar-32 text-primary",
    component: Index,
    layout: "/admin"
  },
  {
    path: "/bookings",
    name: "Bookings",
    icon: "ni ni-collection text-red",
    component: Bookings,
    layout: "/admin"
  },
  {
    path: "/services",
    name: "Services",
    icon: "ni ni-chart-bar-32 text-red",
    component: Services,
    layout: "/admin"
  },
  {
    path: "/create-barber",
    name: "Create Barber",
    icon: "ni ni-check-bold text-green",
    component: Barber,
    layout: "/admin"
  },
  {
    path: "/user-profile",
    name: "User Profile",
    icon: "ni ni-single-02 text-yellow",
    component: Profile,
    layout: "/admin"
  },
  {
    path: "/login",
    name: "Login",
    icon: "ni ni-key-25 text-info",
    component: Login,
    layout: "/auth"
  },
];
export default routes;
