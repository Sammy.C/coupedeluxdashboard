/*!

=========================================================
* Argon Dashboard React - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";

// reactstrap components
import {
  Badge,
  Card,
  CardHeader,
  CardFooter,
  Pagination,
  PaginationItem,
  PaginationLink,
  Table,
  Container,
  Row,
} from "reactstrap";
// core components
import Header from "components/Headers/Header.js";

class Tables extends React.Component {
  render() {
    return (
      <>
        <Header />
        {/* Page content */}
        <Container className="mt--7" fluid>
          {/* Table */}
          <Row>
            <div className="col">
              <Card className="shadow">
                <CardHeader className="border-0">
                  <h3 className="mb-0">Your Bookings</h3>
                </CardHeader>
                <Table className="align-items-center table-flush" responsive>
                  <thead className="thead-light">
                    <tr>
                      <th scope="col">Customer</th>
                      <th scope="col">Date</th>
                      <th scope="col">Status</th>
                      <th scope="col">Service</th>
                      <th scope="col" />
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <th scope="row">
                            <span className="mb-0 text-sm">
                              Jason LaFreniere
                            </span>
                      </th>
                      <td>$35 CAD</td>
                      <td>
                        <Badge color="" className="badge-dot mr-4">
                          <i className="bg-success" />
                          closed
                        </Badge>
                      </td>
                      <td>
                        <div className="avatar-group">
                        <span className="mb-0 text-sm">
                              Hair Cut
                          </span>
                        </div>
                      </td>
                     
                    </tr>
                    <tr>
                      <th scope="row">
                      <span className="mb-0 text-sm">
                              Jean-Michel Cartier
                            </span>
                      </th>
                      <td>$40 CAD</td>
                      <td>
                        <Badge color="" className="badge-dot">
                          <i className="bg-success" />
                          Completed
                        </Badge>
                      </td>
                      <td>
                        <div className="avatar-group">
                        <span className="mb-0 text-sm">
                              Hair Cut & Beard Trim
                          </span>
                        </div>
                      </td>
                      
                    </tr>
                    <tr>
                      <th scope="row">
                          <span className="mb-0 text-sm">
                              Gilbert Gagner
                            </span>
                      </th>
                      <td>$30 CAD</td>
                      <td>
                        <Badge color="" className="badge-dot mr-4">
                          <i className="bg-danger" />
                          Canceled
                        </Badge>
                      </td>     
                      <td>
                        <div className="avatar-group">
                        <span className="mb-0 text-sm">
                              Beard Trim
                          </span>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <th scope="row">
                            <span className="mb-0 text-sm">
                              Jean-Michel Bordeaux
                            </span>
                      </th>
                      <td>$30 CAD</td>
                      <td>
                        <Badge color="" className="badge-dot mr-4">
                          <i className="bg-success" />
                          completed
                        </Badge>
                      </td>
                      <td>
                        <div className="avatar-group">
                        <span className="mb-0 text-sm">
                            Beard Trim
                          </span>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </Table>
                <CardFooter className="py-4">
                  <nav aria-label="...">
                    <Pagination
                      className="pagination justify-content-end mb-0"
                      listClassName="justify-content-end mb-0"
                    >
                      <PaginationItem className="disabled">
                        <PaginationLink
                          href="#pablo"
                          onClick={e => e.preventDefault()}
                          tabIndex="-1"
                        >
                          <i className="fas fa-angle-left" />
                          <span className="sr-only">Previous</span>
                        </PaginationLink>
                      </PaginationItem>
                      <PaginationItem className="active">
                        <PaginationLink
                          href="#pablo"
                          onClick={e => e.preventDefault()}
                        >
                          1
                        </PaginationLink>
                      </PaginationItem>
                      <PaginationItem>
                        <PaginationLink
                          href="#pablo"
                          onClick={e => e.preventDefault()}
                        >
                          2 <span className="sr-only">(current)</span>
                        </PaginationLink>
                      </PaginationItem>
                      <PaginationItem>
                        <PaginationLink
                          href="#pablo"
                          onClick={e => e.preventDefault()}
                        >
                          3
                        </PaginationLink>
                      </PaginationItem>
                      <PaginationItem>
                        <PaginationLink
                          href="#pablo"
                          onClick={e => e.preventDefault()}
                        >
                          <i className="fas fa-angle-right" />
                          <span className="sr-only">Next</span>
                        </PaginationLink>
                      </PaginationItem>
                    </Pagination>
                  </nav>
                </CardFooter>
              </Card>
            </div>
          </Row>
          {/* Dark table */}
          
        </Container>
      </>
    );
  }
}

export default Tables;
