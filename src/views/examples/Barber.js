/*!

=========================================================
* Argon Dashboard React - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";
import {
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  Container,
  Row,
  Col,
  Button
} from "reactstrap";
// core components
import Header from "components/Headers/Header.js";

class Barber extends React.Component {
  state = {};
  render() {
    return (
      <>
        <Header />
        <Container className="mt--2" fluid>
          <Row>
            <Col className="order-xl-1" xl="12">
              <Card className="bg-secondary shadow">
                <CardHeader className="bg-white border-0">
                  <Row className="align-items-center">
                    <Col xs="8">
                      <h3 className="mb-0">Create Barber</h3>
                    </Col>
                  </Row>
                </CardHeader>
                <CardBody>
                  <Form>
                    <h6 className="heading-small text-muted mb-4">
                      User information
                    </h6>
                    <div className="pl-lg-4">
                      <Row>
                        <Col lg="6">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="input-username"
                            >
                              First name 
                            </label>
                            <Input
                              className="form-control-alternative"
                              id="input-username"
                              placeholder="Username"
                              type="text"
                            />
                          </FormGroup>
                        </Col>
                        <Col lg="6">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="input-last-name"
                            >
                              Last Name
                            </label>
                            <Input
                              className="form-control-alternative"
                              id="input-last-name"
                              placeholder="Last name"
                              type="text"
                            />
                          </FormGroup>
                        </Col>
                      </Row>
                      <Row>
                        <Col lg="6">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="input-email"
                            >
                              Email address
                            </label>
                            <Input
                              className="form-control-alternative"
                              id="input-email"
                              placeholder="jesse@example.com"
                              type="email"
                            />
                          </FormGroup>
                        </Col>
                        <Col lg="6">
                          <FormGroup>
                          <label
                              className="form-control-label"
                              htmlFor="input-phone-number"
                            >
                               Phone Number
                            </label>
                            <Input
                              className="form-control-alternative"
                              id="input-phone-number"
                              placeholder="514-000-0000"
                              type="text"
                            />
                          </FormGroup>
                        </Col>
                      </Row>
                      <Row>
                        <Col lg="6">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="input-start-time"
                            >
                               Start Time
                            </label>
                            <Input
                              className="form-control-alternative"
                              id="input-start-time"
                              placeholder="10:00"
                              type="text"
                            />
                          </FormGroup>
                        </Col>
                        <Col lg="6">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="input-end-time"
                            >
                               End Time
                            </label>
                            <Input
                              className="form-control-alternative"
                              id="input-end-time"
                              placeholder="19:00"
                              type="text"
                            />
                          </FormGroup>
                        </Col>
                      </Row>
                      <Row>
                        <Col lg="6">
                          <FormGroup>
                                <label
                                  className="form-control-label"
                                  htmlFor="input-password"
                                >
                                  Password
                                </label>
                                <Input
                                  className="form-control-alternative"
                                  id="input-password"
                                  placeholder="Password"
                                  type="password"
                                />
                            </FormGroup>
                        </Col>
                      </Row>
                    </div>
                    <hr className="my-4" />                  
                    <div className="pl-lg-4">
                    <Row className="justify-content-center">
                       <Col className="lg-12" lg="12">
                        <Button
                            className="mr-4"
                            color="info"
                            href="#pablo"
                            onClick={e => e.preventDefault()}
                            size="m"
                          >
                            Create
                          </Button>
                        </Col>
                        </Row>
                    </div>
                  </Form>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </>
    );
  }
}

export default Barber;
