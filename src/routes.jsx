import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import AdminLayout from "layouts/Admin.js";
import AuthLayout from "layouts/Auth.js";

const Routes = () => {
    return(
        <div>
            <NavBar />
            <Switch>
                <Route path="/" render={props => <AuthLayout {...props} />} />
                <Route path="/admin" render={props => <AdminLayout {...props} />} />
               
            </Switch>
            <Footer />
        </div>
    )
}